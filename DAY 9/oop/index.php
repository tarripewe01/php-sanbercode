<?php

// require('animal.php');
require('frog.php');
require('ape.php');


$sheep = new Animal("shaun");

echo "Name: $sheep->name <br>"; // "shaun"
echo "leg: $sheep->legs <br>"; // 4
echo "cold blooded: $sheep->cold_blooded <br><br>"; // "no"

$sungokong = new Ape("kera sakti");

echo "Name: $sungokong->name <br>";
echo "leg: $sungokong->legs <br>";
echo "cold blooded: $sungokong->cold_blooded <br>";
echo "yell: ";
$sungokong->yell();
echo "<br><br>";


$kodok = new Frog("buduk");

echo "Name: $kodok->name <br>";
echo "leg: $kodok->legs <br>";
echo "cold blooded: $kodok->cold_blooded <br>";
echo "jump: ";
$kodok->jump();
echo "<br><br>";

$numbers = [75, 80, 60];
$numbers[] = 100;
print_r($numbers);

$namaSiswa = 'abduh';
echo strlen($namaSiswa) ;
echo $namaSiswa[strlen($namaSiswa)-1];



