<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function form()
    {
        return view('form');
    }

    public function sapa()
    {
        return "view('sapa')";
    }

    public function sapa_post(Request $request)
    {
        $nama = $request["nama"];
        return "$nama";
    }
}
