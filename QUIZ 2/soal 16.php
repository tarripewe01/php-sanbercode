<!-- < Soal Essay B >

Buatlah sebuah function xo yang menerima satu parameter berupa String. 
Function akan me-return "Benar" jika jumlah karakter x sama dengan jumlah karakter o, 
dan akan me-return "Salah" jika tidak.

NB: bisa menggunakan string method substr_count -->

<?php
function xo($str)
{

    $x = substr_count($str, "x");
    $o = substr_count($str, "o");

    $output = "";

    if ($x == $o) {
        $output .= "Benar <br>";
    } else {
        $output .= "Salah <br>";
    }
    return $output;
}

// Test Cases
echo xo('xoxoxo'); // "Benar"
echo xo('oxooxo'); // "Salah"
echo xo('oxo'); // "Salah"
echo xo('xxooox'); // "Benar"
echo xo('xoxooxxo'); // "Benar"
?>