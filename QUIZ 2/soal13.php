<?php
    // < Soal Essay A >

    // Buatlah sebuah function dengan nama pagar_bintang yang menerima parameter angka. 
    // function tersebut akan mereturn string yang membentuk sebuah "#" jika di baris ganjil 
    // dan "*" jika dibaris genap
    
    
    function pagar_bintang($integer){
        $output = "";
        for ($i = 0; $i < $integer; $i++) {
            $baris = "";
            for ($j = 0; $j < $integer; $j++){
                if ($i % 2 == 0) {
                    $baris .= "#";
                } else {
                    $baris .= "*";
                }
            }
            $output .= $baris . "<br>";
        }
        return $output . "<br>";
    }
    
    echo pagar_bintang(5);
    echo pagar_bintang(8);
    echo pagar_bintang(10);
